Rails.application.routes.draw do
  get 'home/index'

  devise_for :account
  resource :account, controller: 'account'

  resources :services, as: 'services'

  root 'home#index'
end
