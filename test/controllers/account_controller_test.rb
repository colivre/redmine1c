require 'test_helper'

class AccountControllerTest < ActionController::TestCase
  test 'set autologin cookie' do # this is redmine-specific
    account = Account.create!(domain: 'test', name: 'Foo', email: 'foo@example.com', login_token: 'thisisthetoken', password: 'test1234', password_confirmation: 'test1234')
    sign_in account

    get :show
    assert_select 'form > input[name=external_autologin][value=thisisthetoken]'
  end
end
