require 'test_helper'

class ShellScriptSyntaxTest < ActionDispatch::IntegrationTest

  project_files_by_mime_type.each do |file,type|
    if type == 'text/x-shellscript'
      test "syntax of #{file}" do
        assert_equal '', `dash -n #{file} 2>&1`.strip
      end
    end
  end

end
