ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'


class ActionController::TestCase
  include Devise::TestHelpers
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  def assert_valid_attribute(object, attribute)
    object.valid?
    assert !object.errors.keys.include?(attribute.to_sym), "Attribute #{attribute} (value: #{object.send(attribute).inspect})expected to be valid!"
  end

  def assert_invalid_attribute(object, attribute)
    object.valid?
    assert object.errors.keys.include?(attribute.to_sym), "Attribute #{attribute} (value: #{object.send(attribute).inspect})expected to be invalid!"
  end

  def self.project_files_by_mime_type
    @files_by_mime_type ||=
      begin
        filetypes = IO.popen('xargs file --mime-type', 'w+')
        filetypes.puts all_project_files
        filetypes.close_write
        filetypes.readlines.inject({}) do |acc,line|
          parts = line.strip.split(/\s*:\s*/)
	  acc[parts.first] = parts.last
	  acc
        end
      end
  end

  def self.all_project_files
    Dir.glob('**/*').reject do |f|
      f =~ %r{^(tmp|log|pkg)/}
    end
  end

end
