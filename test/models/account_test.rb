require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  # valid domains
  [
    'myteam',
    'my-team',
    'myteam2015'
  ].each do |domain|
    test "should accept #{domain.inspect} as a valid domain" do
      account = Account.new(domain: domain)
      assert_valid_attribute account, :domain
    end
  end

  # invalid domains
  [
    'my team',
    'my_team_with_underscores',
    'my.team',
    'MYTEAM',
    'MyTeam',
    '-myteam',
    '007',
    "two\nlines",
    'foo`date`bar',
    'foo;delete',
    '000-default',
  ].each do |domain|
    test "should accept #{domain.inspect} as a valid domain" do
      account = Account.new(domain: domain)
      assert_invalid_attribute account, :domain
    end
  end

  # Redmine-compatible password encryption
  # SHA1(salt + SHA1(clear_password))
  test 'should use a Redmine-compatible password encryption' do
    account = Account.new
    account.password_salt = 'mysalt' # must come before setting the password
    account.password = 'mypass'

    assert_equal "9a1360d69f82ab9b12493fdef74185ab9e63cc61", account.encrypted_password
  end

  test 'should validate Redmine-compatible password encryption' do
    account = Account.new
    account.password = 'test'
    assert account.valid_password?('test')
  end

end
