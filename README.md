## Development environment

* Install libnss-myhostname >= 228. If you don't have that version, don't
  panic: you can also create several entries for `*.localdomain` in
  /etc/hosts, pointing to 127.0.0.1. Example:

    ```
    foo.localdomain 127.0.0.1
    bar.localdomain 127.0.0.1
    baz.localdomain 127.0.0.1
    ```

    You must have one entry for each test instance you want to create.

* In one terminal, bring up a vagrant machine with `vagrant up`, then ssh into
  it with `vagrant ssh`, then run:

    ```
    $ cd /vagrant
    $ rake db:migrate
    $ foreman start
    ```

* In another terminal, run:

    ```
    $ sudo ./tools/redir
    ```

    (make sure you have the `redir` package installed)

* Access the web app at http://localhost:3000/
