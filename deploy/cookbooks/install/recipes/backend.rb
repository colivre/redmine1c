template '/etc/default/r1c'

cookbook_file '/etc/sudoers.d/laboro' do
  source 'laboro.sudoers'
end

template '/etc/systemd/system/laboro-worker.service' do
  notifies :run, 'execute[systemd-reload]'
end

service 'laboro-worker' do
  provider Chef::Provider::Service::Systemd
  action [:enable, :start]
end

execute 'systemd-reload' do
  action :nothing
  command 'systemctl daemon-reload'
end
