package 'rsync'

source = '/var/tmp/chef.root' # FIXME

base_directory =  '/srv/laboro'
directory base_directory

target = File.join(base_directory, Time.now.strftime('%Y-%m-%d-%H-%M'))

execute 'install' do
  command "rsync -avp --exclude=/deploy --exclude=/log --exclude=/tmp --exclude=/config/database.yml --exclude=/config/secrets.yml --exclude=/db/*.sqlite3 #{source}/ #{target}/ && chown -R root:root #{target}"
end

execute 'install redmine plugins' do
  command [
    "mkdir -p /usr/share/redmine/plugins",
    "ln -sf #{target}/backends/redmine/plugins/* /usr/share/redmine/plugins",
  ].join(' && ')
end

execute 'install redmine themes' do
  command "ln -sf #{target}/backends/redmine/themes/* /usr/share/redmine/public/themes"
end

#######################################################################
# Files that are persistent across deployments
#######################################################################
directory "#{base_directory}/config"
directory "#{base_directory}/db" do
  owner 'root'
  group 'laboro'
  mode 0775
end
template "#{base_directory}/config/database.yml"
user 'laboro'
directory "#{base_directory}/log" do
  owner 'laboro'
end
directory "#{base_directory}/tmp" do
  owner 'laboro'
end
file '/srv/laboro/config/secrets.yml' do
  action :create_if_missing
  content Hash('production' => {'secret_key_base' => SecureRandom.hex(64)}).to_yaml
end

#######################################################################
# symlink persistent files into new deployment
#######################################################################
%w[
  log
  tmp
  config/database.yml
  config/secrets.yml
].each do |f|
  link "#{target}/#{f}" do
    to "#{base_directory}/#{f}"
  end
end

directory "/srv/laboro/ssl" do
  owner 'root'
  group 'laboro'
  mode 0775
end

link "#{target}/ssl" do
  to "/srv/laboro/ssl"
end

#######################################################################
# compile assets
#######################################################################
execute 'rake assets:precompile' do
  cwd target
  environment({ 'RAILS_ENV' => 'production' })
end

execute 'rake db:migrate' do
  cwd target
  user 'laboro'
  group 'laboro'
  environment({ 'RAILS_ENV' => 'production' })
end

#######################################################################
# make new deployment the current one
#######################################################################
link "#{base_directory}/current" do
  to target
end

#######################################################################
# restart application
#######################################################################
execute 'restart' do
  command "touch #{base_directory}/tmp/restart.txt"
end

#######################################################################
# web server configuration
#######################################################################
package 'letsencrypt' do
  if node.platform == 'debian' && node.platform_version < '9'
    options "-t jessie-backports"
  end
end
cookbook_file '/etc/apache2/conf-available/letsencrypt.conf' do
  notifies :reload, 'service[apache2]'
end
execute 'a2enconf letsencrypt.conf'
package 'moreutils'
cron 'letsencrypt renew' do
  command 'for domain in $(ls -1 /srv/laboro/ssl/config/live); do chronic sudo RAILS_ENV=production -u laboro /srv/laboro/current/bin/r1c-letsencrypt $domain; done'
  minute  '11'
  hour    '3'
  day     '*'
  month   '*'
  weekday '*'
end

template '/etc/apache2/sites-available/laboro.conf' do
  source 'apache.conf.erb'
  notifies :reload, 'service[apache2]'
end
cookbook_file '/etc/apache2/sites-available/000-default.conf' do
  notifies :reload, 'service[apache2]'
end
execute 'a2ensite laboro.conf'
