cookbook_file '/etc/apt/trusted.gpg.d/colivre-jessie.gpg' do
  source 'colivre-jessie.gpg.bin'
  notifies :run, 'execute[apt-get update]', :immediately
end
cookbook_file '/etc/apt/preferences.d/laboro' do
  source 'apt_pinning'
end

cookbook_file '/etc/apt/sources.list.d/colivre-jessie.list' do
  notifies :run, 'execute[apt-get update]', :immediately
end
cookbook_file '/etc/apt/sources.list.d/jessie-backports.list' do
  notifies :run, 'execute[apt-get update]', :immediately
end
execute 'apt-get update' do
  action :nothing
end

package 'postfix'
package 'postgresql'
package 'apache2'
service 'apache2' do
  supports reload: true
end
execute 'a2enmod ssl'
package 'libapache2-mod-passenger'
package 'redmine-pgsql'
package 'redmine'

package 'ruby-jbuilder' do
  action :remove
  only_if 'dpkg-query --show ruby-jbuilder'
end

package 'ruby-slim'
package 'ruby-sass-rails'
package 'rabbitmq-server'
package 'amqp-tools'
package 'unicorn'


package 'libjs-jquery'
package 'ruby-foreman'
package 'ruby-devise'
package 'ruby-bootstrap-sass'
