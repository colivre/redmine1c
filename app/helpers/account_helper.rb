module AccountHelper
  def account_service_url(account)
    # FIXME Redmine-specific location
    account_service_protocol + '%s/autologin' % BaseDomain.format(account.domain)
  end

  def account_status(account)
    {
      'created'   => _('Activating ...'),
      'active'    => _('Active'),
      'inactive'  => _('Inactive'),
    }.map do |k,v|
      content_tag('span', v, class: 'status-%s' % k)
    end.join.html_safe
  end

  def account_service_protocol
    Rails.env.production? && 'https://' || 'http://'
  end
end
