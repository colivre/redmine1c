# poll for state change in this account

timeout = 1000

update_status = (account) ->
  if account.status == 'active'
    $('.account-created').hide()
    $('.account-active').show()
    $('input[name=external_autologin]').val(account.login_token)
  else
    setTimeout(poll_status, timeout)

poll_status = ->
  $.getJSON '/account.json', update_status

if $('.account-info').attr('data-status') == 'created'
  poll_status()
