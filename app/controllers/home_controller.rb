class HomeController < ApplicationController
  def index
    if account_signed_in?
      redirect_to account_path
    end
  end
end
