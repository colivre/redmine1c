class AccountController < ApplicationController
  before_action :authenticate_account!
  def show
    respond_with(current_account)
  end
end
