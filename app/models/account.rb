require 'digest/sha1'

class Account < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable,
         :authentication_keys => [:domain]

  validates_presence_of :name, :domain, :status, :email
  validates_uniqueness_of :domain
  validates_format_of :domain, with: /\A[a-z][a-z0-9-]*\z$/, multiline: true
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates_inclusion_of :status, in: ['created', 'active', 'inactive']
  validates_confirmation_of :password
  validates_length_of :password, within: 8..72, on: :create

  after_create do |account|
    request 'add', account.domain
  end

  after_destroy do |account|
    request 'remove', account.domain
  end

  def password=(password)
    self.password_salt = SecureRandom.hex unless password_salt.present?
    super
  end

  def valid_password?(password)
    password_digest(password) == self.encrypted_password
  end

  def full_domain
    BaseDomain.format(domain)
  end

  protected

  # Redmine-compatible password encryption
  # SHA1(salt + SHA1(clear_password))
  def password_digest(password)
    if password_salt.present?
      Digest::SHA1.hexdigest(password_salt + Digest::SHA1.hexdigest(password))
    end
  end

  private

  def request(*args)
    return if Rails.env.test?
    request = File.join(Rails.root, 'bin', 'r1c-request')
    system(request, *args)
  end
end
