class AccountStatus

  def initialize(domain)
    @account = Account.find_by_domain(domain)
  end

  def change_to(status)
    @account.update_attributes!(status: status)
  end

end
