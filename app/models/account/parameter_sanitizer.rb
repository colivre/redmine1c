class Account::ParameterSanitizer < Devise::ParameterSanitizer
  def sign_in
    default_params.permit(:domain, :email, :password)
  end
  def sign_up
    default_params.permit(:domain, :name, :email, :password, :password_confirmation)
  end
end
