class BaseDomain

  def self.name
    ENV.fetch('R1C_BASE_DOMAIN', 'localdomain')
  end

  def self.format(identifier)
    [identifier, name].join('.')
  end

end
