class AddTokenToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :login_token, :string
  end
end
