require 'rake/testtask'

task :test => :test_redmine_plugins


task :test_redmine_plugins do
  if File.directory?('/usr/share/redmine')
    Dir.chdir '/usr/share/redmine' do
      test_loader = Gem.find_files('rake/rake_test_loader').first
      system('sudo', '-u', 'www-data', 'rake', 'db:migrate', 'SCHEMA=/dev/null', 'RAILS_ENV=test')
      system('sudo', '-u', 'www-data', 'ruby', test_loader, *FileList['plugins/*/test/**/*_test.rb'], *ENV.fetch("TESTOPTS", '').split) || fail
    end
  else
    puts "I: redmine not available, not testing Redmine plugins"
  end
end
