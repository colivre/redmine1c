require File.expand_path('../../test_helper', __FILE__)

class PrivateSiteTest < ActionDispatch::IntegrationTest

  test 'private site' do
    Setting.login_required = 1
    get '/robots.txt'
    assert_match "Disallow: /\n", response.body
  end

  test 'public site' do
    Setting.login_required = 0
    get '/robots.txt'
    assert_no_match "Disallow: /\n", response.body
  end

end
