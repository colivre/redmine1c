class PrivateSiteController < ApplicationController

  skip_before_filter :check_if_login_required, :robots
  def robots
    render text: robots_txt, :layout => false, content_type: 'text/plain'
  end

  private

  def robots_txt
    [
      "User-agent: *",
      "Disallow: /"
    ].join("\n") + "\n"
  end

end
