Redmine::Plugin.register :private_site do
  name 'Private Site plugin'
  author 'Author name'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'
end

class PrivateSiteConstraint
  def matches?(request)
    Setting.login_required?
  end
end
RedmineApp::Application.routes.prepend do
  get 'robots.txt', to: 'private_site#robots', constraints: PrivateSiteConstraint.new
end
