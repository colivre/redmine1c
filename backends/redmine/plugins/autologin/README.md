# autologin plugin for Redmine

This plugin manages automatic logins in Redmine. It works by having a Token
with action `external_autologin` for each user who is supposed to have
autologin enabled. Some external application or script must create this token.

Then an external application must generate links to Redmine at `/autologin`,
after setting a cookie called `external_autologin` for the Redmine domain.

When the user that hash the appropriate token in the `external_autologin`
cookie accesses Redmine at `/autologin`, that user will be automatically
loggedn in and redirected to the home page. Of course, such links must only be
present at a wen page that *only* that given user has access, i.e. there must
be a separate authentication process before being able to use that link.
