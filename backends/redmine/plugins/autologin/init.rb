Redmine::Plugin.register :autologin do
  name 'Autologin '
  author 'Antonio Terceiro'
  description 'Automatically log users in by using a special private link'
  version '0.0.1'
  url 'http://colivre.coop.br/'
  author_url 'http://colivre.coop.br/'
end
