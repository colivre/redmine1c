class AutologinController < ApplicationController
  unloadable

  skip_before_filter :check_if_login_required, :check_password_change

  def index
    if params[:external_autologin]
      # TODO token validity?
      if user = Token.find_active_user('external_autologin', params[:external_autologin])
        start_user_session(user)
      end
    end
    redirect_to controller: 'welcome', action: 'index'
  end
end
