require File.expand_path('../../test_helper', __FILE__)

class AutologinControllerTest < ActionController::TestCase
  test 'logs in with a valid token' do
    user = create_user
    token = Token.create!(action: 'external_autologin', user: user)

    post :index, 'external_autologin' => token.value

    assert_response :redirect
    assert_equal user.id, session[:user_id]
  end

  private

  def create_user
    User.new.tap do |user|
      user.login = 'johndoe2'
      user.mail = 'johndoe@example.com'
      user.firstname = 'John'
      user.lastname = 'Doe'
      user.save!
    end
  end
end
